public Listobject<T>[] executeSelectionSortOnArray(Listobject<T>[] array)
{
  // 9/10
  int maxPos = 0;
  for (int i = 0; i < array.length; i++) {
    maxPos = 0;
    for (int k = 1; k < array.length - i ;k++) {
        if (array[k].compareTo(array[maxPos]) > 0) maxPos = k;
    }
    Listobject<T> tmp = array[maxPos];
    array[maxPos] = array[array.length - i - 1];
    array[array.length - i - 1] = tmp;
  }
  return array;
}
