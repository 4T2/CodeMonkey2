/*
Ablauf der Aufrufe der Methoden:
execute(){
  preProcess();
  while (checkBreakCondition()) {
   executeVariant();
   doFunctionality();
  } }
*/
public void preProcess() throws InvalidInputException
{
    G graph = getGraph();
    if(graph == null || getNodeQueue() == null || !graph.getNodeList().containsAll(getNodeQueue()) || !getNodeQueue().containsAll(graph.getNodeList()))
        throw new InvalidInputException();

    ArrayList<Node<N,E>> nodes = graph.getNodeList();

    for(Node<N,E> n : nodes){
        int v = getMatrixIndex(n);
        setM(v,v,graph.getComparator().getZero());

        ArrayList<Edge<N,E>> fanOut = n.getFanOut();
        for(Edge<N,E> e : fanOut){
            if(e.getTargetNode()!= n)
                setM(v,getMatrixIndex(e.getTargetNode()),e.getData());
        }
        for(Node<N,E> m : nodes){
            if(getM(v,getMatrixIndex(m)) == null && m != n)
                setM(v,getMatrixIndex(m),graph.getComparator().getMax());
        }
    }
    setIteration(0);
    setCurrentNode(getNodeQueue().get(0));
}
public boolean checkBreakCondition()
{
    return getIteration() < getGraph().getNodeList().size();
}
public void executeVariant()
{
    setIteration(getIteration()+1);
    if(getIteration() < getNodeQueue().size())
    setCurrentNode(getNodeQueue().get(getIteration()));
}
public void doFunctionality()
{
    G graph = getGraph();
    AbstractEdgeComparator<E> cmp = graph.getComparator();
    ArrayList<Node<N,E>> nodes = graph.getNodeList();

    for(Node<N,E> n : nodes){
        int v = getMatrixIndex(n);
        for(Node<N,E> m: nodes){
            int w = getMatrixIndex(m);
            int u = getMatrixIndex(getCurrentNode());
            E newMin = getM(v,w);
            newMin = cmp.min(newMin, cmp.sum(getM(v,u),getM(u,w)));
            setM(v,w,newMin);
        }
    }
}
