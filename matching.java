public ArrayList<Integer> matching(String S, String T)
{
  ArrayList<Integer> retArr = new ArrayList();
  if(StringHelper.isEmpty(S) || StringHelper.isEmpty(T)) return retArr;
  S = S.toLowerCase();
  T = T.toLowerCase();
  char[] sChar = S.toCharArray();
  char[] tChar = T.toCharArray();
  //go through S as long as there are still enough chars to include T fully
  for (int i = 0; i + tChar.length <= sChar.length;i++) {
    //if potential candidate
    if(tChar[0] == sChar[i]
      && StringHelper.equals(StringHelper.substring(S, i, i + tChar.length),T))
      retArr.add(i+1);
  }
  return retArr;
}
