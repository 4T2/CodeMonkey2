/*
Ablauf der Aufrufe der Methoden:
execute(){
  preProcess();
  while (checkBreakCondition()) {
   executeVariant();
   checkInvariant();
   doFunctionality();
  }
  checkInvariant();
  postProcess();
}
*/
public void preProcess() throws InvalidInputException
{
    G graph = getGraph();
    if(graph == null || graph.getNodeList().size() == 0 || getStartNode() == null)
        throw new InvalidInputException();

    setIterations(0);
    setMst(new ArrayList<Edge<N, E>>());
    HashSet<Node<N, E>> visited = new HashSet<>();
    visited.add(getStartNode());
    setVisited(visited);
    setPriorityQueue( new PriorityQueue<Edge<N,E>>(11, getQueueComparator()));              getPriorityQueue().addAll(getStartNode().getFanOut());
}

public void checkInvariant() throws InvalidInvariantException
{
    if(getMst().size() != getVisited().size()-1)
        throw new InvalidInvariantException();

    if(getVisited().size() != getGraph().getNodeList().size() && getIterations() != getVisited().size())
        throw new InvalidInvariantException();
}
public void executeVariant()
{
    setSmallestEdge(getPriorityQueue().remove());
    setIterations(getIterations()+1);
}
public boolean checkBreakCondition()
{
    return !getPriorityQueue().isEmpty();
}
public void doFunctionality()
{
    Node<N,E> n1 = getSmallestEdge().getSourceNode();
    Node<N,E> n2 = getSmallestEdge().getTargetNode();
    getVisited().add(n1);
    getVisited().add(n2);

    for(Edge<N,E> e : n2.getFanOut()){
        Node<N,E> n3 = e.getTargetNode();
        if(!getVisited().contains(n3))
            getPriorityQueue().add(e);
    }
    removeUnnecessaryEdgesOutOfPriorityQueue();

    getMst().add(getSmallestEdge());

}
