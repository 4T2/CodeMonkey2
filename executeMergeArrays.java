public Listobject<T>[] executeMerge(Listobject<T>[] left, Listobject<T>[] right)
{
  if (left == null || right == null) throw new NullPointerException();
  Listobject<T>[] retArr = new Listobject[left.length + right.length];
  int cmp = 0, i1 = 0, i2 = 0;

  for(int i = 0; i < retArr.length; i++){
    if(i1>=left.length){
         retArr[i] = right[i2];
         i2++;
     }
     else if( i2 >= right.length){
         retArr[i] = left[i1];
         i1++;
     }
     else{
         if(left[i1].compareTo(right[i2])< 0){
             retArr[i] = left[i1];
             i1++;
         }else{
             retArr[i] = right[i2];
             i2++;
         }
     }
  }
  return retArr;
}
