public Node<N, E> addNode(N data)
{
  if (data == null) return null;
  Node<N,E> newNode = new Node<N,E>(getIdGen(), data);
  ArrayList<Node<N,E>> aList = getNodeList();

  aList.add(newNode);
  setNodeList(aList);
  return newNode;
}
