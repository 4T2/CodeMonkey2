public Listobject<T>[] sort(Listobject<T>[] inputdata, ListobjectComparator<T> comp)
{
  for (int i = 0; i < inputdata.length;i++) {
    for (int k = 0; k < inputdata.length -1 -i; k++) {
      int c = comp.compare(inputdata[k], inputdata[k+1]);
      //case swap
      if (c > 0) {
        Listobject<T> tmp = inputdata[k+1];
        inputdata[k+1] = inputdata[k];
        inputdata[k] = tmp;
      }
    }
  }
  return inputdata;
}
