public int countNodesInConnectedGraph(Node<N, E> node)
{
  if(node == null || !contains(node)) return -1;
  HashSet<Node<N, E>> set = new HashSet();
  
  countNodesRec(node, set);
  return set.size();
}

private void countNodesRec(Node<N, E> node, HashSet<Node<N, E>> set)
{
  if(!set.contains(node)) {
      set.add(node);
      for (Edge<N,E> edge: node.getFanOut()) {
          countNodesRec(edge.getTargetNode(),set);
      }
      for (Edge<N,E> edge: node.getFanIn()) {
          countNodesRec(edge.getSourceNode(),set);
      }
  }
}
