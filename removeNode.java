public boolean removeNode(Node<N, E> node)
{
  if (node == null || !getNodeList().contains(node)) return false;
  ArrayList<Node<N, E>> nodeList = getNodeList();
  ArrayList<Edge<N, E>> edgeList = getEdgeList();

  for (Edge<N,E> edge: node.getFanOut()) {
    edge.removeFromNodes();
    edgeList.remove(edge);
  }
  for (Edge<N,E> edge: node.getFanIn()) {
    edge.removeFromNodes();
    edgeList.remove(edge);
  }
  nodeList.remove(node);
  setNodeList(nodeList);
  setEdgeList(edgeList);
  return true;
}
