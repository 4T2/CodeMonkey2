public boolean checkBreakCondition()
{
  return !getPriorityQueue().isEmpty();
}

public void executeVariant()
{
    setSmallestNode(getPriorityQueue().remove());
    getSettled().add(getSmallestNode());
    setIterations(getIterations() + 1);
}
