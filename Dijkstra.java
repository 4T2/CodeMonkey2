/*
Ablauf
execute(){
  preProcess();
  while (checkBreakCondition()) {
   executeVariant();
   checkInvariant();
   doFunctionality();
  }
  checkInvariant();
  postProcess();
}
*/

public void doFunctionality()
{
    E data = getDistances().get(getSmallestNode());

    for(Edge<N,E> e : getSmallestNode().getFanOut()){
        Node<N,E> n = e.getTargetNode();

        if(!getSettled().contains(n)){

            E oldData = getDistances().get(n);
            E newData = getComparator().sum(data,e.getData());

            if(getComparator().compare(newData,oldData) < 0){
                getDistances().put(n,newData);
                getPredecessors().put(n,getSmallestNode());


                getPriorityQueue().remove(n);
                getPriorityQueue().add(n);
                }
            }
        }
    }

}
public boolean checkBreakCondition()
{
    return !getPriorityQueue().isEmpty();
}
public void preProcess() throws InvalidInputException
{
    G graph = getGraph();
    if(graph == null || getSourceNode() == null )
        throw new InvalidInputException("bad input");

    ArrayList<Edge<N,E>> edges = graph.getEdgeList();
    for(int i = 0; i< edges.size();i++){
        if(getComparator().isNegative(edges.get(i).getData()))
            throw new InvalidInputException("bad input");
    }

    setDistances(new HashMap<Node<N,E>,E>());
    for( Node<N,E> n : graph.getNodeList()){
        getDistances().put(n,getComparator().getMax());
    }
    getDistances().put(getSourceNode(),getComparator().getZero());


    setPriorityQueue(new PriorityQueue<Node<N,E>>(11, getQueueComparator()));
    getPriorityQueue().add(getSourceNode());

    setIterations(0);
    setPredecessors(new HashMap<Node<N,E>,Node<N,E>>());
    setSettled(new HashSet<Node<N,E>>());
    setPaths(new HashMap<Node<N,E>, LinkedList<Node<N,E>>>());
}
public void executeVariant()
{
    setIterations(getIterations()+1);
    setSmallestNode(getPriorityQueue().remove());
    getSettled().add(getSmallestNode());

}
public void checkInvariant() throws InvalidInvariantException
{
    if(getPriorityQueue().contains(getSourceNode()))
        throw new InvalidInvariantException("invalid");

    if(getSettled().size() != getIterations())
        throw new InvalidInvariantException("invalid");


    for(Node<N,E> node : settled)
    {
        if(getComparator().compare(getDistances().get(node),getComparator().getMax()) >= 0) throw new InvalidInvariantException();
    }

    for(Node<N,E> node : getGraph().getNodeList())
    {
        if(!getPriorityQueue().contains(node) && !getSettled().contains(node))
        {
            if(getComparator().compare(getDistances().get(node),getComparator().getMax()) != 0) throw new InvalidInvariantException();
        }
    }

}
