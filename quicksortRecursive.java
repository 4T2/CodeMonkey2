public Listobject<T>[] quicksort(Listobject<T>[] array)
{
if (array == null || array.length < 1 || sameInts(array)) return array;

    Listobject<T> pivot = array[0];

    int smaller = 0, same = 1, bigger = 0, len = array.length, cmp;

    for(int i = 1; i < len; i++){
        cmp = array[i].compareTo(pivot);
        if(cmp < 0)
            smaller++;
        else if(cmp == 0)
            same++;
        else
            bigger++;
    }

    Listobject<T>[] smallerArray = new Listobject[smaller];
    Listobject<T>[] sameArray = new Listobject[same];
    Listobject<T>[] biggerArray = new Listobject[bigger];

    same = 0;
    smaller =0;
    bigger =0;

    for(int i = 0; i < len; i++){
        cmp = array[i].compareTo(pivot);
        if(cmp < 0){
            smallerArray[smaller] = array[i];
            smaller++;
        }else if(cmp > 0){
            biggerArray[bigger] = array[i];
            bigger++;
        } else{
            sameArray[same] = array[i];
            same++;
        }
    }
    return combineArrays(quicksort(smallerArray),sameArray,quicksort(biggerArray));
}
