// currently 6/12
public int countEdgesInConnectedGraph(Node<N, E> node)
{
  if(node == null || !contains(node)) return -1;
  HashSet<Edge<N, E>> edgeSet = new HashSet();
  HashSet<Node<N, E>> nodeSet = new HashSet();

  countEdgesRec(node, edgeSet, nodeSet);
  return edgeSet.size();
}

private void countEdgesRec(Node<N, E> node,
    HashSet<Edge<N, E>> edgeSet, HashSet<Node<N, E>> nodeSet)
{
  if(!nodeSet.contains(node)) {
      nodeSet.add(node);
      for (Edge<N,E> edge: node.getFanOut()) {
        if(!edgeSet.contains(edge) && !(edge.hasLinkedEdge() && edgeSet.contains(edge.getLinkedEdge()))){
          edgeSet.add(edge);
          countEdgesRec(edge.getTargetNode(), edgeSet, nodeSet);
          }
      }
      for (Edge<N,E> edge: node.getFanIn()) {
        if(!edgeSet.contains(edge) && !(edge.hasLinkedEdge() && edgeSet.contains(edge.getLinkedEdge()))){
          edgeSet.add(edge);
          countEdgesRec(edge.getSourceNode(), edgeSet, nodeSet);
        }
      }
  }
}
