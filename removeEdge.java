public boolean removeEdge(Edge<N, E> edge)
{
  if (edge == null || !getEdgeList().contains(edge)) return false;
  ArrayList<Edge<N, E>> edgeList = getEdgeList();
  //undirected Part
  if(edge.hasLinkedEdge()) {
    edge.getLinkedEdge().removeFromNodes();
    edgeList.remove(edge.getLinkedEdge());
  }
  //
  edge.removeFromNodes();
  edgeList.remove(edge);
  setEdgeList(edgeList);
  return true;
}
