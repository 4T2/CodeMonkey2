/*
Ablauf der Aufrufe der Methoden:
execute(){
  preProcess();
  while (checkBreakCondition()) {
   executeVariant();
   doFunctionality();
  } }
*/
public void preProcess() throws InvalidInputException
{
    G graph = getGraph();
    if(graph == null)
        throw new InvalidInputException();

    ArrayList<Node<N,E>> nodes = graph.getNodeList();
    setKard_V(nodes.size());
    setM(0);

    Matrix<E> L =  new MatrixFactory<>().newInstance(getKard_V(), getKard_V(), graph.getComparator().getMax());

    for(Node<N,E> n : nodes){
        int v = nodes.indexOf(n)+1;
        L.set(v,v,graph.getComparator().getZero());

        ArrayList<Edge<N,E>> fanOut = n.getFanOut();
        for(Edge<N,E> e : fanOut){
            if(e.getTargetNode()!= n)
                L.set(v,nodes.indexOf(e.getTargetNode())+1,e.getData());
        }
    }
    appendToM(L);
    setL(L);
    setI(-1);
}
public boolean checkBreakCondition()
{
    return getI() < getKard_V();
}
public void executeVariant()
{
    setI(getI()+1);
}
public void doFunctionality()
{
    G graph = getGraph();
    AbstractEdgeComparator<E> cmp = graph.getComparator();
    Matrix<E> M =  new MatrixFactory<>().newInstance(getKard_V(), getKard_V(), cmp.getMax());
    Matrix<E> oldM = getM(getI());
    Matrix<E> L = getL();

    for(int v = 1; v<= getKard_V();v++){
        for(int w = 1; w<=getKard_V();w++){
            E newMin = oldM.get(v,w);
            for(int u = 1; u<=getKard_V();u++){
                newMin = cmp.min(newMin, cmp.sum(oldM.get(v,u),L.get(u,w)));
            }
            M.set(v,w,newMin);
        }
    }
    appendToM(M);
}
