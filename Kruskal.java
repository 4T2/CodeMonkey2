/*
Ablauf der Aufrufe der Methoden:
execute(){
  preProcess();
  while (checkBreakCondition()) {
   executeVariant();
   checkInvariant();
   doFunctionality();
  }
  checkInvariant();
  postProcess();
}
*/
public void preProcess() throws InvalidInputException
{
    G graph = getGraph();
    if(graph == null || graph.getNodeList().size() == 0)
        throw new InvalidInputException("error");

    setIterations(0);
    setMst(new UndirectedGraph<N, E>(graph.getComparator()));
    for(Node<N,E> n : graph.getNodeList()){
        if(n.getFanOut().size() == 0 && n.getFanOut().size() == 0)
            throw new InvalidInputException("error");
        getMst().addNode(n.getData());
    }
    setUnionFind(new UnionFind<N, E>(graph.getNodeList()));
    setPriorityQueue( new PriorityQueue<Edge<N,E>>(11,getQueueComparator()));
    getPriorityQueue().addAll(graph.getEdgeList());

}
public void union(Node<N, E> p, Node<N, E> q)
{
    Node<N,E> rootP = find(p);
    Node<N,E> rootQ = find(q);
    if(rootP == p && rootQ != q)
        getParents().put(rootQ,p);
    else if(rootP != p && rootQ == q)
        getParents().put(rootP,q);
    else if(rootP != p && rootQ != q){
        if(getRanks().get(rootP) > getRanks().get(rootQ))
            getParents().put(rootP,rootQ);
        else
            getParents().put(rootQ,rootP);
    }else
        getParents().put(p,q);

}
public void executeVariant()
{
    setSmallestEdge(getPriorityQueue().remove());
    setIterations(getIterations()+1);
}
public void checkInvariant() throws InvalidInvariantException
{
    if(getPriorityQueue().size() != getGraph().getEdgeList().size() - getIterations())
        throw new InvalidInvariantException("wrong");
    if(UndirectedGraphCycleChecker.hasCycle(getMst()))
        throw new InvalidInvariantException("has cycle");
}
public void doFunctionality()
{
    Edge<N,E> e = getSmallestEdge();
    if(!connected(e.getSourceNode(),e.getTargetNode())){
        try{
	        getMst().addEdge(e.getSourceNode().getId(),e.getTargetNode().getId(),e.getData());
            union(e.getSourceNode(),e.getTargetNode());
        }catch(FanOverflowException f){
            return;
        }
    }
}
public boolean checkBreakCondition()
{
    return !getPriorityQueue().isEmpty();
}
public boolean connected(Node<N, E> p, Node<N, E> q)
{
    return   find(p) == find(q);
}
