public Node<N, E> findNode(Node<N, E> startNode, N data)
{
  if (data == null || startNode == null || !contains(startNode)) return null;
  if (objectEquals(startNode.getData(),data)) return startNode;

  ArrayDeque<Node<N, E>> candidates = new ArrayDeque(50);
  HashSet<Node<N, E>> visited = new HashSet();

  candidates.push(startNode);

  while(!candidates.isEmpty()){
    Node<N, E> current = candidates.pop(); // pick the latest Nodes
    //check all  Nodes that are reachable from the previous one
    visited.add(current);
    if(objectEquals(current.getData(), data)) return current;

    for (Edge<N,E> edge: current.getFanOut()){
      if(!visited.contains(edge.getTargetNode()))
      candidates.push(edge.getTargetNode());
    }
  }
  return null;
}
