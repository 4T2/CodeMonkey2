public int[] binarySearchStep(Listobject<T>[] array, Listobject<T> element, int[] lrm)
{
    if(array == null || element == null)
        throw new NullPointerException();

    int[] l = new int[3];
    int m = (lrm[0] + lrm[1])/2; //get middle and load other values
    l[2] = -1;
    l[0] = lrm[0];
    l[1] = lrm[1];

    if(array.length == 0){
        return l;
    }

    int c = array[m].compareTo(element);
    if(c == 0){ // if elem was found
        l[2] = m;
        return l;
    }
    if(l[1] == m ){ // case objext not in array
        return l;
    }
    if(c > 0){
        l[1] = m-1;
    }else {
        l[0] = m+1;
    }
    l[2] = m;
    return l;
}
