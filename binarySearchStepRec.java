public int binarySearchStep(Listobject<T>[] array, Listobject<T> searchedObject)
{
    if (searchedObject == null || array == null) {
      throw new NullPointerException();
    }
    if(array.length < 1)
        return -1;

    int m = array.length / 2,
        c = searchedObject.compareTo(array[m]);
    if(array.length == 1 && c != 0)
        return -1;
    if(c == 0)
        return m;
    if (c > 0){
        int l = array.length - m - 1;
        Listobject<T>[] newArray = new Listobject[l];
        arraycopyStarter(array,m+1,newArray,0,l ) ;
        int k = binarySearchStep(newArray,searchedObject);
        return (k == -1)? -1: m+1 + k;
    }else{
        int l = array.length - m - ((array.length% 2 == 0)? 0:1);
        Listobject<T>[] newArray = new Listobject[l];
        arraycopyStarter(array,0,newArray,0,l) ;
        return binarySearchStep(newArray,searchedObject);
    }
}
