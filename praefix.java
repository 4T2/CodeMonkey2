public boolean praefix(String a, String b)
{
  if(a == null || b == null) throw new IllegalArgumentException();
  if (StringHelper.isEmpty(a) || StringHelper.isEmpty(b)) return false;
  boolean ret = true;
  char[] aChar = a.toLowerCase().toCharArray();
  char[] bChar = b.toLowerCase().toCharArray();

  for (int i = 0; i < aChar.length; i++){
    if(aChar[i] != bChar[i]) ret = false;
  }
  return ret;
}
