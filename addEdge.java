public void addEdge(Node<N, E> from, Node<N, E> to, E data) throws FanOverflowException
{
  if (from.getFanOut().size() >= getFanOutMax() || to.getFanIn().size() >= getFanInMax()) {
    throw new FanOverflowException("");
  }
  else{
    Edge<N,E> newEdge = new Edge<N,E>(from, to, data);
    /*   -------UNDIRECTED PART----------
    Edge<N,E> newInverseEdge = new Edge<N,E>(to, from, data);
    newEdge.linkTo(newInverseEdge);
    */
    ArrayList<Edge<N,E>> edgeList =  getEdgeList();

    edgeList.add(newEdge);
    setEdgeList(edgeList);
    from.getFanOut().add(newEdge);
    to.getFanIn().add(newEdge);
  }
}
