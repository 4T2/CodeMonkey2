public void doFunctionality()
{
    E data = getDistances().get(getSmallestNode());

    for(Edge<N,E> e : getSmallestNode().getFanOut()){
        Node<N,E> n = e.getTargetNode();

        if(!getSettled().contains(n)){

            E oldData = getDistances().get(n);
            E newData = getComparator().sum(data,e.getData());

            if(getComparator().compare(newData,oldData) < 0){
                getDistances().put(n,newData);
                getPredecessors().put(n,getSmallestNode());


                getPriorityQueue().remove(n);
                getPriorityQueue().add(n);
                }
            }
        }
    }

}
