/*
Ablauf der Aufrufe der Methoden:
execute(){
  preProcess();
  while (checkBreakCondition()) {
   executeVariant();
   doFunctionality();
  }   postProcess(); }
*/

public void preProcess() throws InvalidInputException
{
    G graph = getGraph();

    if(graph == null || graph.getNodeList().size() == 0 || getSourceNode() == null
        || !graph.getNodeList().contains(getSourceNode()) || getTargetNode() == null || !graph.getNodeList().contains(getTargetNode()))
            throw new InvalidInputException("error");

    setPathFound(false);
    setOpenList(new PriorityQueue<Node<N,E>>(11, getQueueComparator()));
    getOpenList().offer(getSourceNode());
    setClosedList(new ArrayList<Node<N, E>>());
    setPredecessorMap(new HashMap<Node<N,E>,Node<N,E>>());
    getPredecessorMap().put(getSourceNode(),null);
    setSourceDistanceMap(new HashMap<Node<N,E>,E>());
    getSourceDistanceMap().put(getSourceNode(),graph.getComparator().getZero());

}
public boolean checkBreakCondition()
{
    return !isPathFound();
}
public void executeVariant()
{
    setCurrentNode(getOpenList().poll());
}
private void expandNode(Node<N, E> node)
{
    for(Edge<N,E> e : node.getFanOut()){
        Node<N,E> n = e.getTargetNode();

        if(!getClosedList().contains(n)){

            E newData = getGraph().getComparator().sum(getSourceDistanceMap().get(node),e.getData());
            E oldData = getSourceDistanceMap().get(n);
            if(!getOpenList().contains(n) || !getGraph().getComparator().greaterEqual(newData,oldData)){
                getPredecessorMap().put(n,node);
                getSourceDistanceMap().put(n,newData);


                getOpenList().offer(n);
                }
            }

        }

    }
}
public void doFunctionality()
{
     if (getCurrentNode() == getTargetNode() || getCurrentNode() == null)
        setPathFound(true);
    if (getCurrentNode() != null) {
        getClosedList().add(getCurrentNode());
        expandNode(getCurrentNode());
    }
}
public void postProcess()
{

    ArrayList<Node<N,E>> path = new ArrayList<>();
    if(getPredecessorMap().containsKey(getTargetNode())){
        Node<N,E> n = getTargetNode();
        while(n != null){
            path.add(n);
            n = getPredecessorMap().get(n);
        }
        reverseCollection(path);
    }

    setPath(path);
}
