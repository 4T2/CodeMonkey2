public MListElement<T> executeMerge(MListElement<T> left, MListElement<T> right, Comparable_Comparator<T> comp)
{
  int c  = comp.compare(left.getKey(),right.getKey());
  MListElement<T> p1; //points to the smaller elem
  MListElement<T> p2; // points to the bigger elem
  if (c < 1) {  // left equal right or left smaller
    p1 = left;
    p2 = right;
  } else { // left bigger
    p2 = left;
    left = right;
    p1 = left;
  }
  while (p1.getNext() != null) {
    if (p2 != null) {
      c = comp.compare(p1.getNext().getKey(), p2.getKey());
      if (c < 1) {
        // next elem of p1 is smaller or equal
        p1 = p1.getNext();
      } else {
        // we need to merge
        right = p1.getNext(); // save the bigger elem
        p1.setNext(p2); //link the to lists
        p2 = right; // set p2 to the bigger
      }
    } else p1 = p1.getNext();
  }
  p1.setNext(p2);
  return left;
}
